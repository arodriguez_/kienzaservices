<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->string('idNew', 36)->primary()->unique();
            $table->string('idNewType', 36);
            $table->foreign('idNewType')->references('idNewType')->on('news_types');
            $table->string('title', 255);
            $table->string('preview', 255);
            $table->longText('body');
            $table->date('endDate');
            $table->tinyInteger('principal');
            $table->string('language', 3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
