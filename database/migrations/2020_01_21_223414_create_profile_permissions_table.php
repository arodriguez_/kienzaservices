<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_permissions', function (Blueprint $table) {
            $table->string('idProfilePermission', 36)->primary()->unique();
            $table->string('idProfile', 36);
            $table->foreign('idProfile')->references('idProfile')->on('profiles');
            $table->string('idPermission', 36);
            $table->foreign('idPermission')->references('idPermission')->on('permissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_permissions');
    }
}
