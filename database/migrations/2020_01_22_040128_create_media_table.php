<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->string('idMedia', 36)->primary()->unique();
            $table->string('idForeign', 36);
            $table->string('idMediaType', 36);
            $table->foreign('idMediaType')->references('idMediaType')->on('media_types');
            $table->string('title', 45);
            $table->string('alt', 45);
            $table->string('resource', 255);
            $table->string('language', 3);
            $table->integer('ordinal');
            $table->tinyInteger('main');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
