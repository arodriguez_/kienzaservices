<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_authors', function (Blueprint $table) {
            $table->string('idNewAuthor', 36)->primary()->unique();
            $table->string('idNew', 36);
            $table->foreign('idNew')->references('idNew')->on('news');
            $table->string('idAuthor', 36);
            $table->foreign('idAuthor')->references('idAuthor')->on('authors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_authors');
    }
}
