<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('idUser', 36)->primary()->unique();
            $table->string('idProfile', 36);
            $table->foreign('idProfile')->references('idProfile')->on('profiles');
            $table->string('username', 30);
            $table->string('password', 255);
            $table->string('name', 40);
            $table->string('lastname', 40);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
