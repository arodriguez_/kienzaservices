<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Banner;
use Faker\Generator as Faker;

$factory->define(Banner::class, function (Faker $faker) {
    return [
        'idBanner' => $faker->unique()->uuid,
        'ordinal' => $faker->randomDigit,
        'startDate' => $faker->dateTime,
        'endDate' => $faker->dateTime,
        'static' => $faker->boolean,
        'active' => $faker->boolean
    ];
});
