<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaTypes extends Model
{
    public $timestamps = false;
}
