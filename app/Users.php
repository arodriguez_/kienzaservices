<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    const USERS_TABLE = 'users';
    public $timestamps = false;
    public $fillable = ['idUser', 'idProfile', 'username', 'password', 'name', 'lastname'];

    public static function getUserData($id=0) {
        if ($id = 0) {
            $value =  //DB::table(self::USERS_TABLE)->orderBy('username', 'asc')->get();
        } else {
            $value = DB::table(self::USERS_TABLE)->where('idUser', $id)->first();
        }
        return $value;
    }

    public static function insertData($data) {
        $value = DB::table(self::USERS_TABLE)->where('username', $data['username'])->get();
        if ($value->count() == 0) {
            DB::table(self::USERS_TABLE)->insert($data);
            return 1;
        } else {
            return 0;
        }
    }

    public static function updateData($id, $data) {
        DB::table(self::USERS_TABLE)->where('idUser', $id)->update($data);
    }

    public static function deleteData($id) {
        DB::table(self::USERS_TABLE)->where('idUser', $id)->delete();
    }
}
