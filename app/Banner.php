<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public $timestamps = false;
    protected $fillable = ['idBanner', 'ordinal', 'startDate', 'endDate', 'static', 'active'];

}
