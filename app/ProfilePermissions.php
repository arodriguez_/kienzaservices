<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilePermissions extends Model
{
    public $timestamps = false;
}
