<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTypes extends Model
{
    public $timestamps = false;
}
