<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsAuthors extends Model
{
    public $timestamps = false;
}
