<?php
namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource {

    public function toArray($request)
    {
        return [
            'idUser' => $this->idUser,
            'idProfile' => $this->idProfile,
            'username' => $this->username,
            'name' => $this->name,
            'lastname' => $this->lastname
        ];
    }
}
