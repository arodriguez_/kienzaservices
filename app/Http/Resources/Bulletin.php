<?php
namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class Bulletin extends JsonResource {
    public function toArray($request)
    {
        return [
            'idBulletin' => $this->idBulletin,
            'title' => $this->title,
            'body' => $this->body,
            'date' => $this->date,
            'sent' => $this->sent
        ];
    }
}
