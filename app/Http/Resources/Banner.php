<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class Banner extends JsonResource {

    public function toArray($request)
    {
        return [
            'id' => $this->idBanner,
            'ordinal' => $this->ordinal,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'static' => $this->static,
            'active' => $this->active
        ];
    }
}
