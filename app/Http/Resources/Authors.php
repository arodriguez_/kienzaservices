<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Authors extends JsonResource{
    public function toArray($request)
    {
        return [
            'idAuthor' => $this->idAuthor,
            'author' => $this->author
        ];
    }
}
