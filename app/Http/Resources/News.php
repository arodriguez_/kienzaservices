<?php
namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class News extends JsonResource {

    public function toArray($request)
    {
        return [
            'idNew' => $this->idNew,
            'idNewType' => $this->idNewType,
            'title' => $this->title,
            'preview' => $this->preview,
            'body' => $this->body,
            'date' => $this->date,
            'principal' => $this->principal,
            'language' => $this->language
        ];
    }
}
