<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    public $timestamps = false;
    protected $fillable = ['description'];
}
